module.exports = {
  reactStrictMode: true,
  images: {
    loader: 'imgix',
    path: 'https://nextjs-avo-paul-jacome.vercel.app',
  },
  rewrites: async () => {
    return [
      { source: '/avocado/:path*', destination: '/product/:path*' }
    ]
  }
}
