import Link from 'next/link';
import styles from './Navbar.module.scss'

const Navbar = () => {
  return (
    <nav>
      <menu className={styles.navbar}>
        <Link href="/">
          <a>Home</a>
        </Link>
        <Link href="/about">
          <a>About Us</a>
        </Link>
        <Link href="/product/pool">
          <a>Product</a>
        </Link>
      </menu>
    </nav>
  )
}

export default Navbar
