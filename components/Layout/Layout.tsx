import Navbar from '@components/Navbar/Navbar';
import { FunctionComponent } from 'react';

const Layout: FunctionComponent = ({ children }) => {
  return (
    <div className="container mx-auto px-4">
      <Navbar/>
      {children}
      <footer>
        this is the footer
      </footer>
    </div>
  )
}

export default Layout
