// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import Database from '@database';
import { NextApiRequest, NextApiResponse } from 'next';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { query } = req;
  const db = new Database();
  const productId = query.productId;
  const avo = await db.getById(productId as string);

  // res.statusCode = 200;
  // res.setHeader('Content-type', 'application/json')
  // res.end(JSON.stringify({ data }))

  res.status(200).json(avo)
}

export default handler
