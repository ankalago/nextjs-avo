// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { IncomingMessage, ServerResponse } from 'http';
import Database from '@database';

const handler = async (req: IncomingMessage, res: ServerResponse) => {
  const db = new Database();
  const data = await db.getAll();
  const length = data.length;

  res.statusCode = 200;
  res.setHeader('Content-type', 'application/json')
  res.end(JSON.stringify({ data, length }))
}

export default handler
