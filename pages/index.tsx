import Image from 'next/image'
import styles from '../styles/Home.module.scss'
import { useEffect, useState } from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-fetch';

const Home = ({ productList }: { productList: TProduct[] }) => {
  const [products, setProducts] = useState<TProduct[]>([]);

  // Client Side
  useEffect(() => {
    fetch('/api/avo').then(res => res.json()).then(({ data, length }) => setProducts(data))
  }, [])

  return (
    <div className={styles.container}>

      <main className={styles.main}>
        <h1 className={`${styles.title} text-5xl font-bold underline`}>
          Welcome to <a href="https://nextjs.org">Next.js!</a>
        </h1>

        <div className="grid grid-cols-3 gap-4">
          {productList.map((product, index) =>
            <div className="border border-gray-500 rounded-lg shadow" key={index}>
              <div className="overflow-hidden rounded-lg">
                <Link href={`/product/${product?.id}`}>
                  <a>
                    <Image src={product?.image} alt={product?.name} width={100} height={100} layout="responsive"/>
                  </a>
                </Link>
              </div>
              <div className="px-2 test-xs font-semibold">{product?.name}</div>
            </div>
          )}
        </div>

      </main>

    </div>
  )
}

// Server Side
// export const getServerSideProps = async () => {
//   const productList: TProduct[] = await fetch('https://nextjs-avo-paul-jacome.vercel.app/api/avo')
//     .then(res => res.json())
//     .then(({ data, length }: { data: TAPIAvoResponse, length: number }) => data)
//   return { props: { productList } }
// }

// Static Side
export const getStaticProps = async () => {
  const productList: TProduct[] = await fetch('https://nextjs-avo-paul-jacome.vercel.app/api/avo')
    .then(res => res.json())
    .then(({ data, length }: { data: TAPIAvoResponse, length: number }) => data)
  return { props: { productList } }
}

export default Home
